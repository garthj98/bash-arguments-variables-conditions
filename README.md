# bash script, variables, arguments and conditions 

This will be a small demo / code along for use to learn hot to use variables and conditions.

By the end we will have a small script that uses these to install nginx on an ubunto machine when given an IP as an argument.

We will talk about the differnce between arguments and variables.

We will also look into using conditions, to help us run our script - using control flow and handle errors. 

## to cover

- script basics 
- running scripts
- escape notifications
- arguments
- variables and user inputs
- conditions 

### script basics 

scripts are sever bash commands written in a file that a bash interperter can exectute.
This is useful to automate tasks and othes.

### running scripts

To run a script it must first be executable. check its permissions using `ll` or `ls -l`. It should have `x` to be executed.

add the permission "execute" by `chmod +x <file>`.

You might need to do this in a remote machine as well when moving or creating new files on the fly.

Also, running it remotely might be easier to call the file using a full `bash` command. However, you can run the file by just pointing to it `./`.

```bash
# to run the file remotley call it using bash 
bash <path/to/file>

# just pointing to the file 
./var_bash.sh

# the dot just represents here you could put the entire path 
~/code/2_week/bash_vars/var_bash.sh 

```

### Escape notation 

In bash and other languages, you have certain characters that behave/signal improtant things other than the actual symbol. For example, `""` are used to outline a string of characters.

How do you print out / echo out some `"`?

***you use what is called an escape character. In bash this is \ ***

```bash
# example 

# in scripts if you want to use arguments they are defined with $<#>, where # is a number
# for example 

echo $1
echo \$1
echo $1

# Also single quotes protect double quotes and double quotes protect single quotes

echo "I'm a cool guy"
echo 'this is double quotes """""""""'

```

### Arguments in scripts 

Arguments are data a function can take in and use internally.

`$ touch <argument>`
or
`$ mv <argument1> <argument2>`

in scripts if you want to use arguments they are defined with $<#>, where # is a number

```bash
# for example 

echo $1

echo "this is argument 1 $1" # needs speach marks not just apostrophe 

```
To call this script with 4 arguments just call the script and pass each argument seperated by spaces

Imagine you want to make a function or script that takes in any number of arguments and does something?

You can use `$*` to represent all the given arguments in a list.

To explain the above we need loops! 

### Loops 

A loop is a block of code that runs for a given number of times. 
this can be useful if you have repetative tasks

```bash

for x in [list]
do 
echo $x
done

```

In a loop a program iterates over the iteratable object (usually a list), substuting in each itteration the x for an object on the list, until there are no more objects.

### Conditions 