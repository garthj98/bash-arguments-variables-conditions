# Conditions evaluate if something is results in true or false
# Thi allows us to control flow - meaning our code will take difffernt actions depending on the inpu t

# syntax 
# if ((<condition>))
# the 
#   block of code
# else 
#   Block of code
# fi

# example 
for COUNTER in 0 3 8 10
do
if (($COUNTER < 5 ))
then 
    echo small
else
    echo big
fi
done

# User input is gathered with read 
read -p "give me a number: " COUNTER
if (($COUNTER < 5 ))
then 
    echo small
else
    echo big
fi