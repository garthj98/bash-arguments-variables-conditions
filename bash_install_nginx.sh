# Create a bash script that takes in arguments and does the installation

read -p "Enter Username (ubuntu or ec2-user): " USER

read -p "Enter Key location (if left blank will use ch9 key): " KEYLOC
if [ -z "$KEYLOC" ]
then 
    KEYLOC=~/.ssh/ch9_shared.pem
fi

IPADDRESS=$1
if [ -z "$IPADDRESS" ]
then 
    read -p "You forgot to enter the IP, please enter: " IPADDRESS
fi


# what happens if i dont give a host name (hostname)
# create an if condiition that checks that script has been called with argument
# if it has, set it to hostnam, else let the user know to call the script with a ip  -o StrickHostKeyChecking=no

ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$IPADDRESS '

if [ "$USER" == "ubuntu" ]
then 

    sudo apt update

    if sudo nginx -v ; 
    then
        echo 'nginx is already installed'
    else
        sudo apt install nginx -y
    fi

    # Create an if condition when you check if nginx already installed. 
    # if installed do nothing 
    # else install 

    sudo systemctl start nginx 

elif [ "$USER" == "ec2-user" ]
then

    # Update redhat
    sudo yum update -y

    if sudo hpptd -v ; 
    then
        echo 'apache is already installed'
    else
        # Install apache
        sudo yum install -y httpd.x86_64
    fi

    # Start apache
    sudo systemctl start httpd.service

    sudo systemctl enable httpd.service

fi

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
Garth
</h1>
<p>
This is a website m8
</p>
<p><button><a href="https://bitbucket.org/garthj98/bash-arguments-variables-conditions/src/master/">This is how I did it</a></button></p>
_END_"
'
open http://$IPADDRESS
