## Bash script for testing arguments and user inputs

# Arguments
# data a function can take in and use internally 
# `$ touch <argument>`
# or
# `$ mv <argument1> <argument2>`

# in scripts if you want to use arguments they are defined with $<#>, where # is a number
# for example 

echo $1
echo \$1
echo $1

# Also single quotes protect double quotes and double quotes protect single quotes

echo "I'm a cool guy"
echo 'this is double quotes """""""""'

# Concatination of strings

echo "this is argument 1 $1" # needs speach marks not just apostrophe 
echo "this is argument 2 $2"
echo "this is argument 3 $3"
echo "this is argument 4 $4"

# To call this script with 4 arguments jsut call the script and pass each argument seperated by spaces
# syntax 
# ./var_bash.sh hi hello hey henlo
# it will ignore additional arguments if added on the end 